#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring JNICALL
Java_id_hackathon_smartdisasterresponse_MainActivity_stringFromJNI(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}
