package id.hackathon.smartdisasterresponse.models.realmlist


/**
 *Created by Ardidy Mulia on 26,Oktober,2018
 */

class KeyValue {

    var key: String = ""
    var value: Int = 0

    constructor()
    constructor(key: String, value: Int) {
        this.key = key
        this.value = value
    }


    override fun toString(): String {
        return key
    }
}