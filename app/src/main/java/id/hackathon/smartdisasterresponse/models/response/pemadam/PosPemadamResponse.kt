package id.hackathon.smartdisasterresponse.models.response.pemadam

data class PosPemadamResponse(
    val count: Int,
    val `data`: ArrayList<DataPemadam>,
    val status: String
)