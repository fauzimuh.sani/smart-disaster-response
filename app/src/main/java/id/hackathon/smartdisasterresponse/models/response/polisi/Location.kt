package id.hackathon.smartdisasterresponse.models.response.polisi

data class Location (
    val latitude: Double,
    val longitude: Double,
    val alamat: String
)