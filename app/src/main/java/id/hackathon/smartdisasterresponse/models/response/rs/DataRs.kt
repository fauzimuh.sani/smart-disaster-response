package id.hackathon.smartdisasterresponse.models.response.rs

import id.hackathon.smartdisasterresponse.models.response.polisi.Location

data class DataRs(
    val email: Any,
    val faximile: List<String>,
    val id: Int,
    val jenis_rsu: String,
    val kode_kecamatan: Int,
    val kode_kelurahan: Long,
    val kode_kota: Int,
    val kode_pos: Int,
    val latitude: Double,
    val location: Location,
    val longitude: Double,
    val nama_rsu: String,
    val telepon: List<String>,
    val website: Any
)