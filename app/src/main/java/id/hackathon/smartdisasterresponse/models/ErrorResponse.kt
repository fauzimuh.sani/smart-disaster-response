package id.hackathon.smartdisasterresponse


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ErrorResponse {
    @SerializedName("error")
    @Expose
    var error: String = ""

    @SerializedName("error_description")
    @Expose
    var errorDesription: String = ""

    @SerializedName("message")
    @Expose
    var message: String = ""
}