package id.hackathon.smartdisasterresponse.models.response.polisi

data class DataPolisi(
    val address: String,
    val lat: Double,
    val lng: Double,
    val name: String,
    val placemark_id: Int
)