package id.hackathon.smartdisasterresponse.models.response.ambulance

data class VEHICLE(
    val COMMAND: String,
    val COUNT: Int,
    val DATA: ArrayList<DATA>,
    val DATA_USER: Any,
    val RESULT: String
)