package id.hackathon.smartdisasterresponse.models

import id.hackathon.smartdisasterresponse.Helpers
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class Hystory : RealmObject {
    @PrimaryKey
    var code: Int = 0
    var desc: String = ""
    var urlPhoto: String = ""
    var status: String = ""

    constructor()
    constructor(code: Int, desc: String, urlPhoto: String, status: String) : super() {
        this.code = code
        this.desc = desc
        this.urlPhoto = urlPhoto
        this.status = status
    }

}


