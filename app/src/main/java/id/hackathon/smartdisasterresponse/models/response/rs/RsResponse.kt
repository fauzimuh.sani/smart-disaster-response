package id.hackathon.smartdisasterresponse.models.response.rs

data class RsResponse(
    val count: Int,
    val `data`: ArrayList<DataRs>,
    val status: String
)