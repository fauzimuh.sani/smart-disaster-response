package id.hackathon.smartdisasterresponse.models.response.pemadam

data class DataPemadam(
    val ALAMAT: String,
    val KELURAHAN: String,
    val LAT: Double,
    val LNG: Double,
    val NO: Int,
    val POS_PEMADAM: String,
    val RT_RW: String
)