package iid.hackathon.smartdisasterresponse.models.realmlist

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class MaterialHeader : RealmObject() {

    @PrimaryKey
    var materialHead : String =""

}