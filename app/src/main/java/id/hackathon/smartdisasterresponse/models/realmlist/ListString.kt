package id.hackathon.smartdisasterresponse.models.realmlist

import io.realm.RealmObject
import io.realm.annotations.RealmClass

/**
 * Created by hendry on 7/21/17.
 */

@RealmClass
open class ListString : RealmObject() {

    var value: String? = null

}