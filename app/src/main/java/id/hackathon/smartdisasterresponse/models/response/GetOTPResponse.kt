package id.hackathon.smartdisasterresponse.models.response

import com.google.gson.annotations.SerializedName
import id.hackathon.smartdisasterresponse.models.Fault

open class GetOTPResponse {
    @SerializedName("fault")
    var fault : Fault? = Fault()
}