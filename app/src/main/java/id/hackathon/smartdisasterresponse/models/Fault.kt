package id.hackathon.smartdisasterresponse.models

class Fault {
    var code: Int = 0
    var message: String = ""
    var description: String = ""
}


