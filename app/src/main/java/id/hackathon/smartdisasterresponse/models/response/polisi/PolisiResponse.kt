package id.hackathon.smartdisasterresponse.models.response.polisi

data class PolisiResponse(
    val category: String,
    val count: Int,
    val `data`: ArrayList<DataPolisi>,
    val status: String
)