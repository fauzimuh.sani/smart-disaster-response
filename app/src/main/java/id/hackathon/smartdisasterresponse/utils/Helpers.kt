package id.hackathon.smartdisasterresponse

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageInfo
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.wifi.WifiInfo
import android.net.wifi.WifiManager
import android.os.Environment
import android.provider.Settings
import android.telephony.TelephonyManager
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.UnderlineSpan
import android.util.Base64
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.AppCompatButton
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.afollestad.materialdialogs.MaterialDialog
import com.bumptech.glide.Glide
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.mikepenz.iconics.IconicsDrawable
import com.mikepenz.iconics.typeface.IIcon
import id.astra.ai.utils.Const
import io.realm.Realm
import io.realm.RealmConfiguration
import okhttp3.OkHttpClient
import java.io.File
import java.math.BigInteger
import java.security.KeyFactory
import java.security.PublicKey
import java.security.spec.RSAPublicKeySpec
import java.security.spec.X509EncodedKeySpec
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern
import javax.crypto.Cipher


object Helpers {

    private lateinit var mProgressDialog: MaterialDialog

    fun showToast(context: Context, message: String, isShowLong: Boolean) {
        if (!TextUtils.isEmpty(message)) {
            Toast.makeText(context, message, if (isShowLong) Toast.LENGTH_LONG else Toast.LENGTH_SHORT).show()
        }
    }

    fun showProgress(v: Context) {
        mProgressDialog = Helpers.showProgressDialog(v, v.getString(R.string.please_wait))
        mProgressDialog.show()
    }

    fun dismissProgress(v: Context) {


        if (mProgressDialog.isShowing) {
            mProgressDialog.dismiss()
        }
    }

    //only can be called in activity
    fun setWindowScrollable(activity: Activity) {
        activity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
    }

    fun getAppVersion(context: Context): String {
        var versionName: String = ""
        try {
            versionName = context.packageManager.getPackageInfo(context.packageName, 0).versionName
        } catch (e: Exception) {
            versionName = "0.0.0.0"
            e.printStackTrace()
        }
        return versionName
    }

    fun setTimeOut(timeOutSeconds: Int): OkHttpClient {
        return OkHttpClient().newBuilder()
            .readTimeout(timeOutSeconds.toLong(), TimeUnit.SECONDS)
            .connectTimeout(timeOutSeconds.toLong(), TimeUnit.SECONDS)
            .readTimeout(timeOutSeconds.toLong(), TimeUnit.SECONDS)
            .build()
    }

    fun getDefaultGson(context: Context): Gson {
        return GsonBuilder().setDateFormat(context.getString(R.string.default_date_time_format))
            .create()
    }

    fun getDefaultGsonCustom(context: Context): Gson {
        return GsonBuilder().setDateFormat(context.getString(R.string.default_date_time_format))
            .setLenient()
            .create()
    }

    fun showMessageDialog(
        context: Context,
        title: String,
        message: String,
        positiveBtnText: String,
        onPositive: MaterialDialog.SingleButtonCallback,
        isCancelAble: Boolean
    ) {
        MaterialDialog.Builder(context)
            .title(title)
            .content(message)
            .positiveText(positiveBtnText)
            .onPositive(onPositive)
            .cancelable(isCancelAble)
            .build().show()
    }

    fun showProgressDialog(context: Context, msg: String): MaterialDialog {
        return MaterialDialog.Builder(context)
            .content(msg)
            .progress(true, 0)
            .cancelable(false)
            .build()
    }

    fun showProgressDialogCustom(context: Context, msg: String): MaterialDialog {
        return MaterialDialog.Builder(context)
            .backgroundColor(ContextCompat.getColor(context, R.color.mtrl_btn_transparent_bg_color))
            .progress(true, 0)
            .cancelable(false)
            .build()
    }


    fun showDialogInfo(
        context: Context,
        @StringRes
        titleRes: Int,
        positiveBtnText: String,
        contentRes: String
    ) {
        MaterialDialog.Builder(context)
            .limitIconToDefaultSize()
            .title(titleRes)
            .content(contentRes)
            .positiveText(positiveBtnText)
            .autoDismiss(true)
            .canceledOnTouchOutside(false)
            .show()
    }

    fun showDialogInfo(
        context: Context,
        @StringRes
        titleRes: Int,
        positiveBtnText: String,
        positiveListener: DialogInterface.OnDismissListener,
        contentRes: String
    ) {
        MaterialDialog.Builder(context)
            .limitIconToDefaultSize()
            .title(titleRes)
            .content(contentRes)
            .positiveText(positiveBtnText)
            .dismissListener(positiveListener)
            .autoDismiss(true)
            .canceledOnTouchOutside(false)
            .show()
    }

    fun showDialogInfo(
        context: Context,
        @StringRes
        titleRes: Int,
        positiveBtnText: String,
        positiveListener: MaterialDialog.SingleButtonCallback,
        isCancelAble: Boolean,
        contentRes: String
    ) {
        MaterialDialog.Builder(context)
            .limitIconToDefaultSize()
            .title(titleRes)
            .content(contentRes)
            .positiveText(positiveBtnText)
            .onPositive(positiveListener)
            .cancelable(isCancelAble)
            .show()
    }

    fun customViewDialog(context: Context, layout: Int, title: String, cancelAble: Boolean): MaterialDialog {

        return MaterialDialog.Builder(context)
            .customView(layout, false)
            .cancelable(cancelAble)
            .build()

    }

    fun customViewDialogScroll(context: Context, layout: Int, title: String, cancelAble: Boolean): MaterialDialog {

        return MaterialDialog.Builder(context)
            .customView(layout, false)
            .cancelable(cancelAble)
            .build()

    }

    fun showMessageDialog(
        context: Context,
        title: String,
        message: String,
        positiveBtnText: String,
        onPositive: MaterialDialog.SingleButtonCallback,
        negativeBtnText: String,
        onNegative: MaterialDialog.SingleButtonCallback,
        isCancelAble: Boolean
    ) {
        MaterialDialog.Builder(context)
            .title(title)
            .content(message)
            .positiveText(positiveBtnText)
            .onPositive(onPositive)
            .negativeText(negativeBtnText)
            .onNegative(onNegative)
            .cancelable(isCancelAble)
            .build()
            .show()
    }

    fun showListDialog(
        context: Context, title: String, body: MutableList<String>,
        listCallback: MaterialDialog.ListCallback, isCancelable: Boolean
    ): MaterialDialog {
        return MaterialDialog.Builder(context)
            .title(title)
            .items(body)
            .itemsCallback(listCallback)
            .cancelable(isCancelable).build()
    }

    fun showListDialogAutoShow(
        context: Context, title: String, contents: MutableList<String>,
        onItemSelected: MaterialDialog.ListCallback,
        cancelAble: Boolean
    ) {
        MaterialDialog.Builder(context)
            .title(title)
            .items(contents)
            .itemsCallback(onItemSelected)
            .cancelable(cancelAble).build()
            .show()

    }

    fun showDialogWithMessage(
        context: Context, title: String, message: String,
        positiveButtonText: String,
        positiveButtonListener: MaterialDialog.SingleButtonCallback,
        isCancelAble: Boolean
    ) {
        MaterialDialog.Builder(context)
            .title(title)
            .content(message)
            .positiveText(positiveButtonText)
            .onPositive(positiveButtonListener)
            .cancelable(isCancelAble)
            .autoDismiss(isCancelAble)
            .build()
            .show()
    }

    fun showListRadioDialog(
        context: Context, title: String, contents: MutableList<String?>,
        positiveBtnText: String,
        negativeBtnText: String, onNegative: MaterialDialog.SingleButtonCallback,
        onItemSelected: MaterialDialog.ListCallbackSingleChoice,
        cancelAble: Boolean
    ) {
        MaterialDialog.Builder(context)
            .title(title)
            .items(contents)
            .itemsCallbackSingleChoice(0, onItemSelected)
            .positiveText(positiveBtnText)
            .negativeText(negativeBtnText)
            .onNegative(onNegative)
            .cancelable(cancelAble)
            .show()
    }


    fun showListDialogWac(
        context: Context, contents: MutableList<String>,
        onItemSelected: MaterialDialog.ListCallback,
        cancelAble: Boolean
    ) {
        MaterialDialog.Builder(context)
            .items(contents)
            .itemsCallback(onItemSelected)
            .cancelable(cancelAble).build()
            .show()

    }

    fun generateGUID(): String {
        return UUID.randomUUID().toString()
    }

    fun setRealmConfiguration(context: Context) {
        Realm.init(context)
        Realm.setDefaultConfiguration(
            RealmConfiguration.Builder()
                .name(context.getString(R.string.realm_file_name))
//                        .encryptionKey(Const.getRealmKey())
                .deleteRealmIfMigrationNeeded()
                .build()
        )
    }


    fun getIcon(
        context: Context, iconId: IIcon, colorResource: Int,
        size: Int
    ): Drawable {
        return IconicsDrawable(context)
            .icon(iconId)
            .color(colorResource)
            .sizeDp(size)
    }

    fun setCompoundIconDropDown(editText: TextInputEditText, context: Context, icon: IIcon) {
        var iconics: Drawable
        iconics = if (icon == null) {
            ActivityCompat.getDrawable(context, R.drawable.abc_ic_arrow_drop_right_black_24dp)!!
        } else {
            Helpers.getIcon(context, icon, ActivityCompat.getColor(context, R.color.text_color_pkb_process), 12)
        }

        editText.setCompoundDrawablesWithIntrinsicBounds(null, null, iconics, null)
    }

    fun setCompoundIconDropDownPKB(editText: TextInputEditText, context: Context, icon: IIcon) {
        var iconics: Drawable
        iconics = if (icon == null) {
            ActivityCompat.getDrawable(context, R.drawable.abc_ic_arrow_drop_right_black_24dp)!!
        } else {
            Helpers.getIcon(context, icon, ActivityCompat.getColor(context, R.color.text_color_pkb_process), 12)
        }

        editText.setCompoundDrawablesWithIntrinsicBounds(null, null, iconics, null)
    }


    fun setCompoundIconButton(mButton: AppCompatButton, context: Context, icon: IIcon, position: String) {

        var iconics: Drawable = if (icon == null) {
            ActivityCompat.getDrawable(context, R.drawable.abc_ic_arrow_drop_right_black_24dp)!!
        } else {
            Helpers.getIcon(context, icon, ActivityCompat.getColor(context, R.color.colorPrimary), 12)
        }


        when (position) {
            "left" -> {
                mButton.setCompoundDrawablesWithIntrinsicBounds(iconics, null, null, null)
            }
            "top" -> {
                mButton.setCompoundDrawablesWithIntrinsicBounds(null, iconics, null, null)
            }
            "right" -> {
                mButton.setCompoundDrawablesWithIntrinsicBounds(null, null, iconics, null)
            }
            "bottom" -> {
                mButton.setCompoundDrawablesWithIntrinsicBounds(null, null, null, iconics)
            }
        }
    }


    fun getVersionInfo(context: Context, type: String): String {
        var result = ""
        var packageInfo: PackageInfo = getPackageInfo(context)
        if (packageInfo != null) {
            result = packageInfo.versionName.toString()
            if (type == "code") {
                result = packageInfo.versionCode.toString()
            }
        }
        return result
    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        var view: View = activity.currentFocus
        if (view == null)
            view = View(activity)
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun setHideKeyboard(activity: Activity) {
        activity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        if (activity.currentFocus != null) {
            val inputMethodManager: InputMethodManager =
                activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(activity.currentFocus.windowToken, 0)
        }
    }


    fun setMainActionBar(actionBar: ActionBar, title: String, buttonback: Boolean) {
        actionBar.setDisplayHomeAsUpEnabled(buttonback)
        if (!TextUtils.isEmpty(title))
            actionBar.title = title
    }

//    fun setMainActionBarCustom(actionBar: android.support.v7.app.ActionBar, title: String, buttonback: Boolean) {
//        actionBar.setDisplayHomeAsUpEnabled(buttonback)
//        actionBar.title = title
//        actionBar.setCustomView(R.layout.toolbar)
//    }

    fun getPackageInfo(context: Context): PackageInfo {
        var result = PackageInfo()
        try {
            result = context.packageManager.getPackageInfo(context.packageName, 0)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return result
    }

    fun setInputLayoutError(inputLayout: TextInputLayout, message: String) {
        inputLayout.error = message
    }

    fun clearInputLayoutError(inputLayout: TextInputLayout) {
        inputLayout.error = null
    }

    @SuppressLint("MissingPermission")
    fun getIMEI(context: Context): String {
        var result = ""
        var telecomManager: TelephonyManager = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        // result = telecomManager.deviceId
        result = "359896060667597"
        return result
    }

    private fun getPublicKey(publicKey: String): PublicKey? {
        var pubKey: PublicKey? = null
        try {
            val publicBytes = android.util.Base64.decode(publicKey, android.util.Base64.DEFAULT)
            val keySpec = X509EncodedKeySpec(publicBytes)
            val keyFactory = KeyFactory.getInstance("RSA")
            pubKey = keyFactory.generatePublic(keySpec)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return pubKey
    }

    fun doEncrypt(value: String): String? {
        var result: String? = null
        try {
            val publicKeyVal = Const.pKey
            val cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding")
            // encrypt the plain text using the public key
            val publicKey = getPublicKey(publicKeyVal)
            cipher.init(Cipher.ENCRYPT_MODE, publicKey)
            val cipherText = cipher.doFinal(value.toByteArray(charset("UTF-8")))
            //set to Base64.NO_WRAP -> remove all new line ('\n')
            result = Base64.encodeToString(cipherText, Base64.DEFAULT)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return result
    }

    fun doEncodeOffline(value: String): String {
        var result: String = ""

        try {
            result = Base64.encodeToString(value.toByteArray(charset("UTF-8")), Base64.DEFAULT)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return result
    }

    fun isInternetConnected(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        val result: Boolean
        result = activeNetwork != null && activeNetwork.isConnectedOrConnecting
        return result
    }


    fun rotateBitmap(source: Bitmap, angle: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(source, 0, 0, source.width, source.height, matrix, true)
    }

    fun getPref(context: Context, prefName: String, key: String): String {
        var pref = context.getSharedPreferences(prefName, 0)
        return pref.getString(key, "")
    }

    fun getPrefBool(context: Context, prefName: String, key: String): Boolean {
        var pref = context.getSharedPreferences(prefName, 0)
        return pref.getBoolean(key, false)
    }

    fun getPrefInt(context: Context, prefName: String, key: String): Int {
        var pref = context.getSharedPreferences(prefName, 0)
        return pref.getInt(key, 0)
    }

    fun savePref(context: Context, prefName: String, key: String, value: String?) {
        val pref = context.getSharedPreferences(prefName, 0)
        var editor = pref.edit()
        editor.putString(key, value)
        editor.commit()
    }

    fun savePref(context: Context, prefName: String, key: String, value: Boolean?) {
        val pref = context.getSharedPreferences(prefName, 0)
        var editor = pref.edit()
        editor.putBoolean(key, false)
        editor.commit()
    }

    fun savePref(context: Context, prefName: String, key: String, value: Int?) {
        val pref = context.getSharedPreferences(prefName, 0)
        var editor = pref.edit()
        if (value != null) {
            editor.putInt(key, value)
        }
        editor.commit()
    }


    fun getShownTimeHourFromString(stringDate: String?): String {
        var result = "00.00"
        if (stringDate == null)
            return result
        if (stringDate != "") {
            if (stringDate == "00:00:00") {
                return result
            }
            try {
                var sf = SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss",
                    Locale.US
                )
                val tempDate = sf.parse(stringDate)
                if (tempDate != null) {
                    sf = SimpleDateFormat("HH:mm:ss", Locale.US)
                }
                result = sf.format(tempDate)
            } catch (e: java.text.ParseException) {
                e.printStackTrace()
            }

        }
        return result
    }


    fun string2Date(date: String, pattern: String): Date? {
        try {
            return SimpleDateFormat(pattern, Locale.US).parse(date)
        } catch (ignore: Exception) {
            ignore.printStackTrace()
        }

        return null
    }

    fun date2String(date: Date, pattern: String): String {
        try {
            return SimpleDateFormat(pattern, Locale.US).format(date)
        } catch (ignore: Exception) {
            ignore.printStackTrace()
            return ""
        }
    }


    fun getDateTimeNowMinute(): String {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH) + 1
        val day = c.get(Calendar.DAY_OF_MONTH)
        val hour = c.get(Calendar.HOUR_OF_DAY)
        val minute = c.get(Calendar.MINUTE)
        val second = 0

        val today = (year.toString() + "-" + pad(month) + "-" + pad(day) + " "
                + pad(hour) + ":" + pad(minute) + ":" + pad(second))
        return today
    }

    fun pad(c: Int): String {
        return if (c >= 10)
            c.toString()
        else
            "0" + c.toString()
    }

    fun compareTime(targetDate: Date?, checkDate: Date?): Boolean {
        var isValid = false
        if (targetDate != null && checkDate != null) {
            if (checkDate.after(targetDate)) {
                isValid = true
            }
        }
        return isValid
    }


    // remove "0" in .0 or .300 decimal format
    fun formateDecimal(value: Double?): String {
        val formatter = DecimalFormat("0.#")
        return formatter.format(value)
    }

    fun convertDateFormat(stringDate: String, recentFormat: String, wantedFormat: String): String {
        val date = Helpers.string2Date(stringDate, recentFormat)
        val dateFor = SimpleDateFormat(wantedFormat)
        return dateFor.format(date)

    }

    fun removeInputLayoutError(inputLayout: TextInputLayout) {
        inputLayout.error = ""
    }

    fun isLocationEnabled(context: Context): Boolean {
        var locationMode = 0
        try {
            locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE)
        } catch (e: Settings.SettingNotFoundException) {
            e.printStackTrace()
        }

        return locationMode != Settings.Secure.LOCATION_MODE_OFF
    }

    fun compareTime8to5(): Boolean {
        var isValid = false
        val pattern = "HH:mm"
        // valid jika time after 8.00 n before 17.00
        val time8clock = Helpers.string2Date("08:00", pattern)
        val time17clock = Helpers.string2Date("17:00", pattern)
        val c = Calendar.getInstance()
        val hour = c.get(Calendar.HOUR_OF_DAY)
        val minute = c.get(Calendar.MINUTE)
        val timeNow = Helpers.string2Date(hour.toString() + ":" + minute, pattern)
        if (timeNow != null && time8clock != null && time17clock != null) {
            if (timeNow!!.after(time8clock)) {
                if (timeNow!!.before(time17clock)) {
                    isValid = true
                }
            }
        }
        return isValid
    }

    fun getShownTimeFromString(stringDate: String): String {

        var result = ""
        if (stringDate == null)
            return result
        if (stringDate != "") {
            if (stringDate == "00:00:00") {
                return result
            }
            try {
                var sf = SimpleDateFormat(
                    "yyyy-MM-dd'T'HH:mm:ss",
                    Locale.US
                )
                val tempDate = sf.parse(stringDate)
                if (tempDate != null) {
                    sf = SimpleDateFormat("HH:mm", Locale.US)
                }
                result = sf.format(tempDate)
            } catch (e: java.text.ParseException) {
                e.printStackTrace()
            }

        }
        return result

    }


    fun getResizedBitmap(bm: Bitmap, newHeight: Int, newWidth: Int): Bitmap {
        val width = bm.width
        val height = bm.height
        val scaleWidth = newWidth.toFloat() / width
        val scaleHeight = newHeight.toFloat() / height
        // CREATE A MATRIX FOR THE MANIPULATION
        val matrix = Matrix()
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight)

        // "RECREATE" THE NEW BITMAP
        val resizedBitmap = Bitmap.createBitmap(
            bm, 0, 0, width, height,
            matrix, false
        )
        return resizedBitmap
    }

    fun cropString(data: String, awal: Int, akhir: Int): String {
        var print = ""
        val Length = (Math.floor((akhir - awal).toDouble()) / 10).toInt()
        if (data.length > Length) {
            print = print + data.substring(0, Length)
        } else {
            print = print + data
        }

        return print
    }

    fun getDrawable(context: Context, resDrawable: Int): Drawable? {
        return ContextCompat.getDrawable(context, resDrawable)
    }

    fun getColor(context: Context, resDrawable: Int): Int {
        return ContextCompat.getColor(context, resDrawable)
    }

    fun getDateNowByPatterns(pattern: String): String {
        val dateNow = Calendar.getInstance().time
        val df = SimpleDateFormat(pattern)
        var result = ""

        result = df.format(dateNow)
        //result = "2018-08-07"

        return result
    }

    fun getTimeNowByParameters(parameter: Int): String {
        var dateNow = Calendar.getInstance().time
        val df = SimpleDateFormat("HH:mm:ss")
        var result = ""
        val calendar = Calendar.getInstance()
        calendar.time = dateNow
        calendar.add(Calendar.HOUR, parameter)
        dateNow = calendar.time

        result = df.format(dateNow)

        return result
    }

    fun getDateNowByPatternsByParameters(pattern: String, parameter: Int): String {
        var dateNow = Calendar.getInstance().time
        val df = SimpleDateFormat(pattern)
        var result = ""
        val calendar = Calendar.getInstance()
        calendar.time = dateNow
        calendar.add(Calendar.DATE, parameter)
        dateNow = calendar.time

        result = df.format(dateNow)

        return result
    }

    fun handleZeroText(etCStockQuantity: EditText, qty: String) {

        if (qty == "00") {
            etCStockQuantity.setText("0")
            etCStockQuantity.selectAll()
        }
        if (qty == "01") {
            etCStockQuantity.setText("1")
            etCStockQuantity.selectAll()
        }
        if (qty == "02") {
            etCStockQuantity.setText("2")
            etCStockQuantity.selectAll()
        }
        if (qty == "03") {
            etCStockQuantity.setText("3")
            etCStockQuantity.selectAll()
        }
        if (qty == "04") {
            etCStockQuantity.setText("4")
            etCStockQuantity.selectAll()
        }
        if (qty == "05") {
            etCStockQuantity.setText("5")
            etCStockQuantity.selectAll()
        }
        if (qty == "06") {
            etCStockQuantity.setText("6")
            etCStockQuantity.selectAll()
        }
        if (qty == "07") {
            etCStockQuantity.setText("7")
            etCStockQuantity.selectAll()
        }
        if (qty == "08") {
            etCStockQuantity.setText("8")
            etCStockQuantity.selectAll()
        }
        if (qty == "09") {
            etCStockQuantity.setText("9")
            etCStockQuantity.selectAll()
        }

    }

    fun getShownDateFromString(stringDate: String): String {

        var result = ""
        if (stringDate == null)
            return result
        if (stringDate != "") {
            if (stringDate == "00:00:00") {
                return result
            }
            try {
                var sf = SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss",
                    Locale.US
                )
                val tempDate = sf.parse(stringDate)
                if (tempDate != null) {
                    sf = SimpleDateFormat("dd-MM-yyyy", Locale.US)
                }
                result = sf.format(tempDate)
            } catch (e: java.text.ParseException) {
                e.printStackTrace()
            }

        }
        return result

    }

    fun convertToRupiah(value: Double?): String {
        if (value != null) {
            val localeID = Locale("in", "ID")
            val formatRupiah = NumberFormat.getCurrencyInstance(localeID)
            return formatRupiah.format(value)
        } else {
            return ""
        }
    }

    fun getShownDateFromStringEdit(stringDate: String): String {

        var result = ""
        if (stringDate == null)
            return result
        if (stringDate != "") {
            if (stringDate == "00:00:00") {
                return result
            }
            try {
                var sf = SimpleDateFormat(
                    "yyyy-MM-dd'T'HH:mm:ss",
                    Locale.US
                )
                val tempDate = sf.parse(stringDate)
                if (tempDate != null) {
                    sf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                }
                result = sf.format(tempDate)
            } catch (e: java.text.ParseException) {
                e.printStackTrace()
            }

        }
        return result

    }

    fun getDirectory(foldername: String): String {
        val directory: File
        directory = File(
            Environment.getExternalStorageDirectory().absolutePath
                    + File.separator + foldername
        )
        if (!directory.exists()) {
            directory.mkdirs()
        }
        return directory.absolutePath
    }

    fun getShownDateFromStringEdit2(stringDate: String): String {

        var result = ""
        if (stringDate == null)
            return result
        if (stringDate != "") {
            if (stringDate == "00:00:00") {
                return result
            }
            try {
                var sf = SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss",
                    Locale.US
                )
                val tempDate = sf.parse(stringDate)
                if (tempDate != null) {
                    sf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                }
                result = sf.format(tempDate)
            } catch (e: java.text.ParseException) {
                e.printStackTrace()
            }

        }
        return result

    }

    fun customViewDialogScroll(
        context: Context,
        layout: Int,
        title: String,
        cancelAble: Boolean,
        positiveBtnText: String,
        onPositive: MaterialDialog.SingleButtonCallback,
        negativeBtnText: String,
        onNegative: MaterialDialog.SingleButtonCallback
    ): MaterialDialog {

        return MaterialDialog.Builder(context)
            .customView(layout, false)
            .cancelable(cancelAble)
            .positiveText(positiveBtnText)
            .onPositive(onPositive)
            .negativeText(negativeBtnText)
            .onNegative(onNegative)
            .build()

    }


    fun regexValidation(email: String, pattern: String): Boolean {
        val pattern = Pattern.compile(pattern)
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }

    fun convertDoubleToDecimal(value: String): String {
        val valueDouble = value.toDouble()

        val format: NumberFormat = NumberFormat.getInstance()
        format.maximumFractionDigits = 0
        val currency: Currency = Currency.getInstance("IDR")
        format.currency = currency
        return format.format(valueDouble).replace(",", ".")
    }

    @JvmStatic
    fun sleep(ms: Int) {
        try {
            Thread.sleep(ms.toLong())
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

    }

    fun setDateNow(indicator: String): String {

        var result = ""
        var df = SimpleDateFormat("dd-MM-yyyy")
        var tf = SimpleDateFormat("HH:mm:ss")
        var ff = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        var cust = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        var date = df.format((Calendar.getInstance().time))
        var time = tf.format((Calendar.getInstance().time))
        var full = ff.format((Calendar.getInstance().time))
        var custResult = cust.format((Calendar.getInstance().time))
        if (indicator.toLowerCase().equals("time")) {
            result = time
        } else if (indicator.toLowerCase().equals("date")) {
            result = date
        } else if (indicator.toLowerCase().equals("full")) {
            result = full
        } else if (indicator.toLowerCase().equals("custom")) {
            result = custResult
        }

        return result

    }

    fun setStringUnderline(value: String): SpannableString {
        val content = SpannableString(value)
        content.setSpan(UnderlineSpan(), 0, value.length, 0)
        return content
    }

    fun convertStringDatebyPatterns(dateString: String, patternOld: String, patterTo: String): String {
        return try {
            val dfOld = SimpleDateFormat(patternOld, Locale.US)
            val dfTo = SimpleDateFormat(patterTo, Locale.US)
            val date = dfOld.parse(dateString)
            var result = ""
            result = dfTo.format(date)
            result
        } catch (e: Exception) {
            e.printStackTrace()
            ""
        }
    }

//    fun showDialogRelogin(context: Context, userID: String,
//                          callbackOk: MaterialDialog.SingleButtonCallback,
//                          callbackCancel: MaterialDialog.SingleButtonCallback): MaterialDialog? {
//        val dialog = MaterialDialog.Builder(context)
//                .title(context.getString(R.string.re_login))
//                .customView(R.layout.relogin_form, true)
//                .positiveText(R.string.caption_process)
//                .autoDismiss(false)
//                .cancelable(false)
//                .positiveColor(ContextCompat.getColor(context, R.color.md_black_1000))
//                .onPositive(callbackOk)
//                .negativeText(R.string.caption_close)
//                .negativeColor(ContextCompat.getColor(context, R.color.md_black_1000))
//                .onNegative(callbackCancel)
//                .build()
//        val username = dialog.findViewById(R.id.username) as TextInputEditText
//        username.setText(userID)
////        dialog.show()
//        return dialog
//    }

    fun convertDrawableToBitmap(drawable: BitmapDrawable): Bitmap {
        var result: Bitmap
        result = drawable.bitmap
        return result
    }

    fun resizeBitmap(bitmap: Bitmap, newWidth: Int, newHeight: Int): Bitmap? {
        val resized = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true)
        return resized
    }

    fun convertViewToBitmap(view: View): Bitmap? {
        //Define a bitmap with the same size as the view
        val returnedBitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
        //Bind a canvas to it
        val canvas = Canvas(returnedBitmap)
        //Get the view's background
        val bgDrawable = view.background
        if (bgDrawable != null)
        //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        else
        //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE)
        // draw the view on the canvas
        view.draw(canvas)
        //return the bitmap
        return returnedBitmap
    }

    fun setFrameLayout(fragmentManager: FragmentManager, resId: Int, targetFragment: Fragment) {
        fragmentManager
            .beginTransaction()
            .replace(resId, targetFragment, targetFragment::class.java.simpleName)
            .commit()
    }

    fun loadImage(context: Context, imageView: ImageView, url: String) {
        Glide.with(context).load(url)
            .into(imageView)
    }

    fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor {
        var vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
        vectorDrawable!!.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        var bitmap = Bitmap.createBitmap(
            vectorDrawable.getIntrinsicWidth(),
            vectorDrawable.getIntrinsicHeight(),
            Bitmap.Config.ARGB_8888
        );
        var canvas = Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    fun setMainActionBarCustom(actionBar: ActionBar, title: String, buttonback: Boolean) {
        actionBar.setDisplayHomeAsUpEnabled(buttonback)
        actionBar.title = title
        actionBar.setCustomView(R.layout.toolbar)
    }
}
