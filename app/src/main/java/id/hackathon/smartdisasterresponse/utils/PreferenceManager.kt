package id.hackathon.smartdisasterresponse.utils

import android.content.Context
import com.securepreferences.SecurePreferences
import id.hackathon.smartdisasterresponse.R


open class PreferenceManager private constructor() {

    var securePreferences: SecurePreferences? = null
    var accessToken: String? = null
    var refresh_Token: String? = null
    var headOfficeCode: String? = null

    init {
    }

    private object Holder {
        val instances = PreferenceManager()
    }

    companion object {
        val instance: PreferenceManager by lazy { Holder.instances }
    }

    private fun getSecurePreference(preferences: PreferenceManager, context: Context): SecurePreferences? {
        if (preferences.securePreferences == null) {
            preferences.securePreferences = SecurePreferences(context)
        }
        return preferences.securePreferences
    }

    fun clearPreference(preferences: PreferenceManager, context: Context) {
        val secureSharedPreferences = preferences.getSecurePreference(preferences, context)
        secureSharedPreferences!!.edit().clear().apply()
    }

    fun getValueFromSecurePreferences(preferences: PreferenceManager,
                                      keyword: String, context: Context): String? {
        var values: String? = null
        val secureSharedPreferences = preferences.getSecurePreference(preferences, context)
        when (keyword) {
            context.getString(R.string.secure_preferences_access_token) -> {
                if (preferences.accessToken == null) {
                    preferences.accessToken = secureSharedPreferences!!.getString(keyword, "")
                }
                values = preferences.accessToken
            }
            context.getString(R.string.secure_preferences_refresh_token) -> {
                if (preferences.refresh_Token == null) {
                    preferences.refresh_Token = secureSharedPreferences!!.getString(keyword, "")
                }
                values = preferences.refresh_Token
            }
            context.getString(R.string.secure_preferences_head_office_code) -> {
                if (preferences.headOfficeCode == null) {
                    preferences.headOfficeCode = secureSharedPreferences!!.getString(keyword, "")
                }
                values = preferences.headOfficeCode
            }

        }

        return values
    }

    fun setValueToSecurePreferences(preferences: PreferenceManager,
                                    keyword: String, value: String, context: Context) {
        try {
            val secureSharedPreferences = preferences.getSecurePreference(preferences, context)
            secureSharedPreferences!!.edit().putString(keyword, value).apply()
            when (keyword) {
                context.getString(R.string.secure_preferences_access_token) -> {
                    preferences.accessToken = value
                }
                context.getString(R.string.secure_preferences_refresh_token) -> {
                    preferences.refresh_Token = value
                }
                context.getString(R.string.secure_preferences_head_office_code) -> {
                    preferences.headOfficeCode = value
                }
            }
        } catch (ex: Exception) {
            throw ex
        }
    }
}

