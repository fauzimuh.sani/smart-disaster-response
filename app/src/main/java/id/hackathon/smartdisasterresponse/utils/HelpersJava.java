package id.hackathon.smartdisasterresponse.utils;

import android.content.Context;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import id.astra.hso.stowadig.utils.jsonParser.IntegerRealmListConverter;
import id.astra.hso.stowadig.utils.jsonParser.StringRealmListConverter;
import id.hackathon.smartdisasterresponse.R;
import id.hackathon.smartdisasterresponse.models.realmlist.ListInt;
import id.hackathon.smartdisasterresponse.models.realmlist.ListString;
import io.realm.RealmList;


import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by hendry on 7/26/17.
 */

public class HelpersJava {


    public static String doHash256Hex(String data) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        try {
            md.update(data.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return bytesToHex(md.digest());
    }

    private static String bytesToHex(byte[] bytes) {
        StringBuffer result = new StringBuffer();
        for (byte byt : bytes)
            result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));
        return result.toString();
    }

    public static Gson getCustomGson(Context context) {
        return new GsonBuilder()
                .setDateFormat(context.getString(R.string.default_date_time_format))
                .registerTypeAdapter(new TypeToken<RealmList<ListInt>>() {
                        }.getType(),
                        new IntegerRealmListConverter())
                .registerTypeAdapter(new TypeToken<RealmList<ListString>>() {
                        }.getType(),
                        new StringRealmListConverter())
                .create();
    }

    public static Gson getCustomGson2(Context context) {
        return new GsonBuilder()
                .setLenient()
                .setDateFormat(context.getString(R.string.default_date_time_format))
                .registerTypeAdapter(new TypeToken<RealmList<ListInt>>() {
                        }.getType(),
                        new IntegerRealmListConverter())
                .registerTypeAdapter(new TypeToken<RealmList<ListString>>() {
                        }.getType(),
                        new StringRealmListConverter())
                .create();
    }


}
