package id.astra.hso.stowadig.utils.jsonParser

import com.google.gson.JsonArray
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import id.hackathon.smartdisasterresponse.models.realmlist.ListInt

import java.lang.reflect.Type

import io.realm.RealmList

/**
 * Created by hendry on 7/31/17.
 */

class IntegerRealmListConverter : JsonSerializer<RealmList<ListInt>>, JsonDeserializer<RealmList<ListInt>> {

    override fun serialize(src: RealmList<ListInt>, typeOfSrc: Type,
                           context: JsonSerializationContext): JsonElement {
        val ja = JsonArray()
        for (tag in src) {
            ja.add(context.serialize(tag))
        }
        return ja
    }

    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type,
                             context: JsonDeserializationContext): RealmList<ListInt> {
        val realmList = RealmList<ListInt>()
        val ja = json.asJsonArray
        for (je in ja) {
            val value = context.deserialize<Int>(je, Int::class.java)
            val listInt = ListInt()
            listInt.value = value
            realmList.add(listInt)
        }
        return realmList
    }

}
