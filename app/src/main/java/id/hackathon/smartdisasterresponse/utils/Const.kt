package id.astra.ai.utils

import android.os.Environment
import java.io.File

/**
 * Created by hendry on 7/31/17.
 */

object Const {

    init {
        System.loadLibrary("native-lib")
    }

    enum class Stage(val value: Int) {
        PRODUCTION(1),
        STAGING(2),
        DEVELOPMENT(3)
    }

    enum class Status(val value: Int) {
        OPEN(1),
        CONFIRM(3),
        ON_THE_WAY(4),
        ARRIVED(5),
        JOB_STARTED(6),
        JOB_FINISHED(7),
        BILLED(8),
        RELEASED(14),
        COMPLETED(16),
        PARTIALLY_BILLED(17)
    }

    enum class Job_Status(val value: Int) {
        OPEN(0),
        IN_PROGRESS(1),
        PAUSE(2),
        FINISH(4),
    }


    //Change this to prod, dev, or staging
    val environmentStage = Stage.DEVELOPMENT.value

    private external fun getUrl(environmentStage: Int): String
    private external fun getUrlLogin(environmentStage: Int): String
    private external fun getPubKey(environmentStage: Int): String
    private external fun getFirstSHA256(environmentStage: Int): String
    private external fun getSecondSHA256(environmentStage: Int): String

    val url = getUrl(environmentStage)
    val urlLogin = getUrlLogin(environmentStage)
    val pKey = getPubKey(environmentStage)
    val firstSha256 = getFirstSHA256(environmentStage)
    val secondSha256 = getSecondSHA256(environmentStage)

    val MODULE_NAME = "MobileRecruitment"
    val FOLDER_TEMP_WAC_PKB = "ILSR"
    val QUALITY_COMPRESSION = 30

    val DIRECTORY_WAC_THS = (Environment
        .getExternalStorageDirectory().toString()
            + File.separator
            + MODULE_NAME
            + File.separator
            + FOLDER_TEMP_WAC_PKB
            + File.separator)

    external fun realmKey(): ByteArray

}
