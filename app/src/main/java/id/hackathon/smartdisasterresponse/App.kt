package id.hackathon.smartdisasterresponse

import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import id.hackathon.smartdisasterresponse.Helpers

class App : MultiDexApplication() {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        Helpers.setRealmConfiguration(this)
    }
}