package id.hackathon.smartdisasterresponse.networking

import id.hackathon.smartdisasterresponse.models.response.ambulance.AmbulanceResponse
import id.hackathon.smartdisasterresponse.models.response.polisi.PolisiResponse
import id.hackathon.smartdisasterresponse.models.response.GetOTPResponse
import id.hackathon.smartdisasterresponse.models.response.pemadam.PosPemadamResponse
import id.hackathon.smartdisasterresponse.models.response.rs.RsResponse
import io.reactivex.Observable

import retrofit2.http.*


interface NetworkService {

    @GET("emergency/pospolda")
    fun getPolisi(): Observable<PolisiResponse>

    @GET("emergency/ambulance")
    fun getDataAmbulance(): Observable<AmbulanceResponse>

    @GET("rumahsakitumum")
    fun getDataRs(): Observable<RsResponse>

    @GET("emergency/pospemadam")
    fun getDataPemadam(): Observable<PosPemadamResponse>

    @GET("082240206034/")
    fun getOTP(): Observable<GetOTPResponse>

}
