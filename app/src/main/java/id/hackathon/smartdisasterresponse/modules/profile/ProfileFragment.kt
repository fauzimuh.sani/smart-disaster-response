package id.hackathon.smartdisasterresponse.modules.profile

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.hackathon.smartdisasterresponse.R
import kotlinx.android.synthetic.main.fragment_profile.*
import id.hackathon.smartdisasterresponse.Helpers
import kotlinx.android.synthetic.main.blank_toolbar.*

class ProfileFragment : Fragment() {

    //class
    private lateinit var mContext: Context
    private lateinit var toolbar: androidx.appcompat.widget.Toolbar


    //data
    private var urlPhoto = "https://cdn.soccerwiki.org/images/player/85260.png"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mContext = context!!

        toolbar_title.text = "Profile"
        progressBar.progressTintList = ColorStateList.valueOf(Color.BLUE)

        initWidget()
    }

    private fun setAdapter() {

    }

    private fun initWidget() {
        Helpers.loadImage(mContext, iv_photo_profile, urlPhoto)

    }
}