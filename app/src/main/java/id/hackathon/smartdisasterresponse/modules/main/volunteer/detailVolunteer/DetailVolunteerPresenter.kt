package id.hackathon.smartdisasterresponse.modules.main.volunteer.detailVolunteer

import android.widget.Toast
import id.hackathon.smartdisasterresponse.models.response.ambulance.AmbulanceResponse
import id.hackathon.smartdisasterresponse.models.response.polisi.PolisiResponse
import id.hackathon.smartdisasterresponse.models.response.GetOTPResponse
import id.hackathon.smartdisasterresponse.modules.main.volunteer.VolunteerFragment
import id.hackathon.smartdisasterresponse.networking.NetworkModule
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.lang.Exception

class DetailVolunteerPresenter(val v: DetailVolunteer) {
    private var service: NetworkModule? = null
    private val mCompositeDisposable: CompositeDisposable = CompositeDisposable()

    fun hitGetOTP() {
        val numberPhone = "081374146327"
        try {
            this.service = NetworkModule(
                v, null, "http://3be9a450.ngrok.io/api/getotp/",
                "vH5IApExeaUn+jOMvs1M1c/1evpHHQmVE9lKoMSkEKFcGY2DXeHCkk6Zcn85BY/Y"
            )
            val responseObservable = service!!.getPreparedObservable(
                service!!.service.getOTP(), GetOTPResponse::class.java, false, false
            )

            mCompositeDisposable.add(
                responseObservable
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ this.handleResponse(it) }, { this.handleResponse(it) })
            )

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun handleResponse(response: Any) {
        if (response is GetOTPResponse) {
            v.onSuccess()
        } else if (response is Throwable) {
            v.onError(response.message!!)
        }
    }

}