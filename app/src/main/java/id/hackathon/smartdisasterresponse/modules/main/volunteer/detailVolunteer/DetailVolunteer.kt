package id.hackathon.smartdisasterresponse.modules.main.volunteer.detailVolunteer

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toolbar
import id.hackathon.smartdisasterresponse.Helpers
import id.hackathon.smartdisasterresponse.R
import id.hackathon.smartdisasterresponse.modules.login.otp.OTPActivity
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_detail_volunteer.*
import kotlinx.android.synthetic.main.blank_toolbar.*
import kotlinx.android.synthetic.main.item_volunteer.view.*

class DetailVolunteer : AppCompatActivity() {

    private lateinit var toolbar: androidx.appcompat.widget.Toolbar
    private lateinit var presenter: DetailVolunteerPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_volunteer)
        presenter = DetailVolunteerPresenter(this)
        toolbar = findViewById(R.id.toolbarDetail)
        setSupportActionBar(toolbar)

        val actionbar = supportActionBar

        Helpers.setMainActionBarCustom(actionbar!!, " ", true)
        toolbar_title.text = "Detail Volunteer"


        Helpers.loadImage(
            this,
            img_event,
            "http://strategidanbisnis.com/uploads/artikel/93e2b9b4109c1480e3eb91af790dee23.jpg"
        )

        request_join.setOnClickListener {
            //toOTP()
            presenter.hitGetOTP()
        }
    }

    fun onSuccess() {
        toOTP()
    }

    fun onError(message: String) {
        showDialogMessage(message)
    }

    private fun toOTP() {
        val intent = Intent(this, OTPActivity::class.java)
        startActivity(intent)
    }

    private fun showDialogMessage(message: String) {
        Helpers.showDialogInfo(this, R.string.alert, getString(R.string.ok), message)
    }
}
