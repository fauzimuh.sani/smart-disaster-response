package id.hackathon.smartdisasterresponse.modules.review

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.hackathon.smartdisasterresponse.R
import io.realm.Realm

class ReviewAdapter(private val list: List<String>,
                     private val realm: Realm,
                     private val context: Context,
                     private val itemClick: (String) -> Unit)
    : RecyclerView.Adapter<ReviewAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemView: View = LayoutInflater.from(parent!!.context).inflate(R.layout.item_review, parent, false)
        var holderMaterialReqAdapter: Holder =
            Holder(
                itemView,
                itemClick
            )
        holderMaterialReqAdapter.setIsRecyclable(false)
        return holderMaterialReqAdapter
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(list[position], position, realm,context)
    }

    class Holder(val view: View,
                 private val itemClick: (String) -> Unit ): RecyclerView.ViewHolder(view)
    {

        fun bind(data: String, position: Int, realm: Realm, context: Context) {
            with(data) {



                //                itemView.cv_forum.setCardBackgroundColor(androidx.core.content.ContextCompat.getColor(context, R.color.text_color_blue_back))


            }

        }
    }
}