package id.hackathon.smartdisasterresponse.modules.login.otp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.core.content.ContextCompat
import id.hackathon.smartdisasterresponse.Helpers
import id.hackathon.smartdisasterresponse.R
import kotlinx.android.synthetic.main.activity_otp.*

class OTPActivity : AppCompatActivity() {

    var isTwoClickDigit1 = true
    var isTwoClickDigit2 = true
    var isTwoClickDigit3 = true
    var isTwoClickDigit4 = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp)

        setListener()
    }

    private fun setListener() {
        bt_send.setOnClickListener {
            bt_send.text = "Applied"
            bt_send.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.button_radius_grey))
        }

        et_digit_1.setOnKeyListener { view, i, keyEvent ->
            if (i == 67 && et_digit_1.text.toString().isBlank()) {
                Helpers.hideKeyboard(this)
                return@setOnKeyListener true
            } else {
                return@setOnKeyListener false
            }
        }
        et_digit_2.setOnKeyListener { view, i, keyEvent ->
            if (i == 67 && et_digit_2.text.toString().isBlank()) {
                et_digit_1.requestFocus()
                return@setOnKeyListener true
            } else {
                return@setOnKeyListener false
            }
        }
        et_digit_3.setOnKeyListener { view, i, keyEvent ->
            if (i == 67 && et_digit_3.text.toString().isBlank()) {
                et_digit_2.requestFocus()
                return@setOnKeyListener true
            } else {
                return@setOnKeyListener false
            }
        }
        et_digit_4.setOnKeyListener { view, i, keyEvent ->
            if (i == 67 && et_digit_4.text.toString().isBlank()) {
                et_digit_3.requestFocus()
                return@setOnKeyListener true
            } else {
                return@setOnKeyListener false
            }
        }

        et_digit_1.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0!!.isNotBlank()) {
                    isTwoClickDigit1 = false
                    et_digit_2.requestFocus()
                }
            }

        })

        et_digit_2.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0!!.isNotBlank()) {
                    isTwoClickDigit2 = false
                    et_digit_3.requestFocus()
                }
            }

        })

        et_digit_3.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0!!.isNotBlank()) {
                    isTwoClickDigit3 = false
                    et_digit_4.requestFocus()
                }
            }

        })

        et_digit_4.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                var isTwoClick = false
                if (p0!!.isNotBlank()) {
                    Helpers.hideKeyboard(this@OTPActivity)
                }
            }

        })
    }
}
