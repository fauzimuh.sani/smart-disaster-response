package id.hackathon.smartdisasterresponse.modules.main.home

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import id.hackathon.smartdisasterresponse.modules.main.home.ambulance.AmbulanceFragment
import id.hackathon.smartdisasterresponse.modules.main.home.pemadam.PemadamFragment
import id.hackathon.smartdisasterresponse.modules.main.home.polisi.PolisiFragment
import id.hackathon.smartdisasterresponse.modules.main.home.rumahSakit.RumahSakitfragment
import id.hackathon.smartdisasterresponse.modules.profile.ProfileFragment

class HomePagerAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return PolisiFragment()
            1 -> return PemadamFragment()
            2 -> return RumahSakitfragment()
            3 -> return AmbulanceFragment()
            else -> return PolisiFragment()
        }
    }

    override fun getCount(): Int {

       return 4
    }

}