package id.hackathon.smartdisasterresponse.modules.history

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_history.*
import id.hackathon.smartdisasterresponse.modules.history.HistoryAdapter
import id.hackathon.smartdisasterresponse.R
import id.hackathon.smartdisasterresponse.models.Hystory

class HistoryFragment : Fragment() {
    private lateinit var realm: Realm
    private lateinit var presenter: HystoryPresenter
    private var listHystory: MutableList<Hystory> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        realm = Realm.getDefaultInstance()
        presenter = HystoryPresenter(this, realm)

        addHystory()
        setAdapter(listHystory)


    }

    private fun addHystory() {
        listHystory.add(
            Hystory(
                0,
                "Bencana Banjir dan Tanah Longsor di Purworejo",
                "http://www.hariandepok.com/wp-content/uploads/2016/06/sar-637x402.jpg",
                "COMPLETE!!!"
            )
        )
        listHystory.add(
            Hystory(
                1,
                "Tragedi Jatuhnya Pesawat Lion Air JT 610 di Tanjung Kerawang",
                "https://cdn0-a.production.vidio.static6.com/uploads/video/image/1504302/pesawat-lion-air-jt-610-jatuh-di-tanjung-karawang-515565.jpg",
                "PENDING"
            )
        )

    }

    private fun setAdapter(list: List<Hystory>) {

        var adapter = HistoryAdapter(
            list,
            realm,
            this.requireContext()
        ) {

        }
        rv_history.layoutManager = LinearLayoutManager(this.requireContext(), RecyclerView.VERTICAL, false)
        rv_history.setHasFixedSize(true)
        rv_history.isFocusable = false
        adapter.notifyDataSetChanged()
        rv_history.adapter = adapter

    }

}