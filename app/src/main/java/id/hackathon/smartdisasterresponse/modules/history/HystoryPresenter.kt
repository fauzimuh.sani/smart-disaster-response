package id.hackathon.smartdisasterresponse.modules.history

import android.widget.Toast
import id.hackathon.smartdisasterresponse.models.Hystory
import id.hackathon.smartdisasterresponse.networking.NetworkModule
import id.hackathon.smartdisasterresponse.utils.realmhelper.ModelHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import java.lang.Exception

class HystoryPresenter(val v: HistoryFragment, realm: Realm) {
    private var realm = realm
    private var service: NetworkModule? = null
    private val mCompositeDisposable: CompositeDisposable = CompositeDisposable()

    fun getAllHystory(): MutableList<Hystory>? {
        var result: MutableList<Hystory>? = mutableListOf()
        if (ModelHelper().getAll(realm, Hystory::class.java) != null) {
            result!!.addAll(realm.copyFromRealm(ModelHelper().getAll(realm, Hystory::class.java)))
        }
        return result
    }
}