package id.hackathon.smartdisasterresponse.modules.main

import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import id.hackathon.smartdisasterresponse.Helpers
import id.hackathon.smartdisasterresponse.R
import id.hackathon.smartdisasterresponse.models.Hystory
import id.hackathon.smartdisasterresponse.modules.history.HistoryFragment
import id.hackathon.smartdisasterresponse.modules.main.volunteer.VolunteerFragment
import id.hackathon.smartdisasterresponse.modules.main.home.HomeFragment
import id.hackathon.smartdisasterresponse.modules.profile.ProfileFragment
import id.hackathon.smartdisasterresponse.modules.review.ReviewFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var presenter:MainPresenter
    private var listHystory : MutableList<Hystory> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter = MainPresenter(this)

        // Example of a call to a native method
        val intent = intent
        val message = intent.getStringExtra("message")
        if (!message.isNullOrEmpty()) {
            AlertDialog.Builder(this)
                .setTitle("Notification")
                .setMessage(message)
                .setPositiveButton("Ok", { dialog, which -> }).show()
        }

        addHystory()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = Helpers.getColor(applicationContext, R.color.color_blue_light)
        }

        bottom_nav.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {

                R.id.menu_services -> {
                    Helpers.setFrameLayout(supportFragmentManager, R.id.content, HomeFragment())
                }
                R.id.menu_volunteer -> {
                    Helpers.setFrameLayout(supportFragmentManager, R.id.content, VolunteerFragment())
                }

                R.id.menu_profile -> {
                    Helpers.setFrameLayout(supportFragmentManager, R.id.content, ProfileFragment())
                }

                R.id.menu_history -> {
                    Helpers.setFrameLayout(supportFragmentManager, R.id.content, HistoryFragment())
                }

                R.id.menu_review -> {
                    Helpers.setFrameLayout(supportFragmentManager, R.id.content, ReviewFragment())
                }

            }
            true
        }
        bottom_nav.selectedItemId = R.id.menu_services


    }

    private fun addHystory() {
        listHystory.add(Hystory(0,"Bencana Banjir dan Tanah Longsor di Purworejo","http://www.hariandepok.com/wp-content/uploads/2016/06/sar-637x402.jpg","COMPLETE!!!"))
        listHystory.add(Hystory(1,"Tragedi Jatuhnya Pesawat Lion Air JT 610 di Tanjung Kerawang","https://cdn0-a.production.vidio.static6.com/uploads/video/image/1504302/pesawat-lion-air-jt-610-jatuh-di-tanjung-karawang-515565.jpg","PENDING"))
        presenter.saveAllHystory(listHystory)
    }
}
