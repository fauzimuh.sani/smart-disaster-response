package id.hackathon.smartdisasterresponse.modules.login

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.fastaccess.permission.base.PermissionHelper
import com.fastaccess.permission.base.callback.OnPermissionCallback
import com.mikepenz.community_material_typeface_library.CommunityMaterial
import id.hackathon.smartdisasterresponse.Helpers
import id.hackathon.smartdisasterresponse.R
import id.hackathon.smartdisasterresponse.modules.login.register.RegisterActivity
import id.hackathon.smartdisasterresponse.modules.main.MainActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), OnPermissionCallback {

    //class
    private lateinit var mContext: Context
    private val PERMISSION = arrayOf(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.BLUETOOTH
    )
    private lateinit var permission: PermissionHelper

    //data
    private var email = ""
    private var password = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        mContext = this
        permission = PermissionHelper.getInstance(this)

        initWidget()
        setListener()
    }

    private fun initWidget() {
        Helpers.setCompoundIconDropDown(input_email, mContext, CommunityMaterial.Icon.cmd_at)
        Helpers.setCompoundIconDropDown(input_password, mContext, CommunityMaterial.Icon.cmd_lock)

    }

    private fun setListener() {
        bt_login.setOnClickListener {
            doLogin()
        }
        tv_register.setOnClickListener {
            toRegisterMenu()
        }
    }

    private fun doLogin() {
        val isSuccessValidation = doValidation()
        if (isSuccessValidation) {
            if (permissionGranted()) {
                toHomeMenu()
            } else {
                requestPermission()
            }

        }
    }

    private fun toHomeMenu() {
        val intent = Intent(mContext, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun toRegisterMenu() {
        val intent = Intent(mContext, RegisterActivity::class.java)
        startActivity(intent)
    }

    private fun doValidation(): Boolean {
        email = input_email.text.toString()
        password = input_password.text.toString()

        if (email.isBlank()) {
            showDialogMessage("Username Tidak Boleh Kosong!")
            return false
        } else if (password.isBlank()) {
            showDialogMessage("Password Tidak Boleh Kosong!")
            return false
        } else {
            if (email == "admin" && password == "admin") {
                return true
            } else {
                showDialogMessage("Username atau Password Salah!")
                return false
            }

        }
    }

    private fun showDialogMessage(message: String) {
        Helpers.showDialogInfo(mContext, R.string.alert, getString(R.string.ok), message)
    }

    /*Permission*/
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        permission.onActivityForResult(requestCode)

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        permission.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onPermissionPreGranted(permissionsName: String) {

    }

    override fun onPermissionGranted(permissionName: Array<out String>) {
        toHomeMenu()
    }

    override fun onNoPermissionNeeded() {
    }

    override fun onPermissionReallyDeclined(permissionName: String) {
    }

    override fun onPermissionDeclined(permissionName: Array<out String>) {
    }

    override fun onPermissionNeedExplanation(permissionName: String) {
    }

    private fun permissionGranted(): Boolean {
        return ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        permission
            .setForceAccepting(true) // default is false. its here so you know that it exists.
            .request(PERMISSION)
    }

}