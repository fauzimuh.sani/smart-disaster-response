package id.hackathon.smartdisasterresponse.modules.review

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_history.*
import id.hackathon.smartdisasterresponse.R
import kotlinx.android.synthetic.main.fragment_review.*

class ReviewFragment : Fragment() {
    private lateinit var realm: Realm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_review, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        realm = Realm.getDefaultInstance()

        var list: MutableList<String> = mutableListOf<String>()
        list.add("")
        setAdapter(list)


    }

    private fun setAdapter(list: List<String>) {

        var adapter = ReviewAdapter(
            list,
            realm,
            this.requireContext()
        ) {

        }
        rv_review.layoutManager = LinearLayoutManager(this.requireContext(), RecyclerView.VERTICAL, false)
        rv_review.setHasFixedSize(true)
        rv_review.isFocusable = false
        adapter.notifyDataSetChanged()
        rv_review.adapter = adapter

    }

}