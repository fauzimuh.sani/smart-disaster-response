package id.hackathon.smartdisasterresponse.modules.history

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.hackathon.smartdisasterresponse.Helpers
import id.hackathon.smartdisasterresponse.R
import id.hackathon.smartdisasterresponse.models.Hystory
import io.realm.Realm
import kotlinx.android.synthetic.main.item_history.view.*

class HistoryAdapter(private val list: List<Hystory>,
                   private val realm: Realm,
                   private val context: Context,
                   private val itemClick: (String) -> Unit)
    : RecyclerView.Adapter<HistoryAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemView: View = LayoutInflater.from(parent!!.context).inflate(R.layout.item_history, parent, false)
        var holderMaterialReqAdapter: Holder =
            Holder(
                itemView,
                itemClick
            )
        holderMaterialReqAdapter.setIsRecyclable(false)
        return holderMaterialReqAdapter
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(list[position], position, realm,context)
    }

    class Holder(val view: View,
                 private val itemClick: (String) -> Unit ): RecyclerView.ViewHolder(view)
    {

        fun bind(data: Hystory, position: Int, realm: Realm, context: Context) {
            with(data) {

                itemView.tv_desc.text = data.desc
                itemView.tv_status.text = data.status

                Helpers.loadImage(
                    context,
                    itemView.iv_item_photo,
                    data.urlPhoto
                )

//                itemView.cv_forum.setCardBackgroundColor(androidx.core.content.ContextCompat.getColor(context, R.color.text_color_blue_back))
//
//                itemView.setOnClickListener {
//                    itemClick(this)
//                }


            }

        }
    }
}