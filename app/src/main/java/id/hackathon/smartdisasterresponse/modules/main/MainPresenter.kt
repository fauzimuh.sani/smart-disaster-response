package id.hackathon.smartdisasterresponse.modules.main

import android.widget.Toast
import id.hackathon.smartdisasterresponse.models.Hystory
import id.hackathon.smartdisasterresponse.models.response.GetOTPResponse
import id.hackathon.smartdisasterresponse.modules.main.volunteer.VolunteerFragment
import id.hackathon.smartdisasterresponse.networking.NetworkModule
import id.hackathon.smartdisasterresponse.utils.realmhelper.ModelHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.lang.Exception

class MainPresenter(val v: MainActivity) {
    private var service: NetworkModule? = null
    private val mCompositeDisposable: CompositeDisposable = CompositeDisposable()

    fun saveAllHystory(list: MutableList<Hystory>){
        ModelHelper().addAll(list)
    }

}