package id.hackathon.smartdisasterresponse.modules.main.volunteer

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.hackathon.smartdisasterresponse.R
import kotlinx.android.synthetic.main.fragment_volunteer.*
import id.hackathon.smartdisasterresponse.modules.main.volunteer.detailVolunteer.DetailVolunteer

class VolunteerFragment : Fragment() {

    //class

    private lateinit var mContext: Context

    //data
    private lateinit var adapter: VolunteerAdapter
    private var listVolunteer: MutableList<String> = mutableListOf()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_volunteer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mContext = context!!

        setList()
        initWidget()
        setAdapter()
    }

    private fun setList() {
        listVolunteer.add("")
        listVolunteer.add("")
    }

    private fun setAdapter() {
        adapter = VolunteerAdapter(listVolunteer) { s ->
            val intent = Intent(this.requireContext(), DetailVolunteer::class.java)
            startActivity(intent)
        }
        adapter.notifyDataSetChanged()
        rv_volunteer.adapter = adapter
    }

    private fun initWidget() {
        //setRV
        rv_volunteer.layoutManager = LinearLayoutManager(mContext, RecyclerView.VERTICAL, false)
        rv_volunteer.setHasFixedSize(true)
        rv_volunteer.isFocusable = false


    }
}
