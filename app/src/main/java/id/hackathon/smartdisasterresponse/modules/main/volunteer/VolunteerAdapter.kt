package id.hackathon.smartdisasterresponse.modules.main.volunteer

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mikepenz.community_material_typeface_library.CommunityMaterial
import id.hackathon.smartdisasterresponse.Helpers
import id.hackathon.smartdisasterresponse.R
import kotlinx.android.synthetic.main.item_volunteer.view.*


class VolunteerAdapter(private val datalist: MutableList<String>, private val listener: (String) -> Unit) :
    RecyclerView.Adapter<ViewHolder>() {

    lateinit var mContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_volunteer, parent, false)
        mContext = parent.context
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return datalist.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bindItem(mContext, datalist[position], listener, position)
    }

}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {


    fun bindItem(context: Context, data: String, listener: (String) -> Unit, position: Int) {

//        cardParent.setOnClickListener {
//            listener(assignment)
//        }
        var urlPhoto = ""
        if (position == 1) {
            itemView.tv_desc.text = "Tanag Longsor Menerjang Kota Bogor!"
            itemView.tv_location.text = "Bogor, Jawa Barat"
            itemView.tv_item_tcash.text = "Rp 0"
            itemView.tv_item_tmoney.text = "Rp 200.000"
            itemView.tv_people.text = "56"
            urlPhoto = "http://pusatkrisis.kemkes.go.id/__asset/__images/content/78ilustrasi_longsor_garut_png.jpg"
        } else {
            urlPhoto = "http://strategidanbisnis.com/uploads/artikel/93e2b9b4109c1480e3eb91af790dee23.jpg"
        }

        Helpers.loadImage(
            context,
            itemView.iv_item_photo,
            urlPhoto
        )

        itemView.setOnClickListener {
            listener("")
        }
        itemView.iv_item_location.setImageDrawable(
            Helpers.getIcon(
                context,
                CommunityMaterial.Icon.cmd_map_marker,
                R.color.blue_button,
                16
            )
        )

    }
}