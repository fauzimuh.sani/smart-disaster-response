package id.hackathon.smartdisasterresponse.modules.main.home.polisi

import android.widget.Toast
import id.hackathon.smartdisasterresponse.models.response.polisi.PolisiResponse
import id.hackathon.smartdisasterresponse.networking.NetworkModule
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.lang.Exception

class PolisiPresenter(val v: PolisiFragment) {
    private var service: NetworkModule? = null
    private val mCompositeDisposable: CompositeDisposable = CompositeDisposable()

    fun hitDataPolisi(){
        try {
            this.service = NetworkModule(
                v.requireContext(), null, "https://api.jakarta.go.id/v1/",
                "vH5IApExeaUn+jOMvs1M1c/1evpHHQmVE9lKoMSkEKFcGY2DXeHCkk6Zcn85BY/Y"
            )
            val responseObservable = service!!.getPreparedObservable(
                service!!.service.getPolisi(), PolisiResponse::class.java, false, false
            )

            mCompositeDisposable!!.add(responseObservable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ this.handleResponse(it) }, { this.handleResponse(it) }))

        } catch (e: Exception){
            e.printStackTrace()
        }
    }

    fun handleResponse(response: Any){
        if (response is PolisiResponse){
            v.onResult(response.data)
//            Toast.makeText(v.requireContext(), response.count, Toast.LENGTH_SHORT).show()
        } else if (response is Throwable) {
//            Toast.makeText(v.requireContext(), response.message, Toast.LENGTH_SHORT).show()
        }
    }

}