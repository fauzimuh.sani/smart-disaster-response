package id.hackathon.smartdisasterresponse.modules.main.home.pemadam

import id.hackathon.smartdisasterresponse.Helpers
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

import id.hackathon.smartdisasterresponse.R
import id.hackathon.smartdisasterresponse.models.response.pemadam.DataPemadam
import id.hackathon.smartdisasterresponse.utils.GPSTracker
import kotlinx.android.synthetic.main.fragment_pemadam.*

class PemadamFragment : Fragment(), OnMapReadyCallback {

    private lateinit var gps: GPSTracker
    private lateinit var presenter: PemadamPresenter
    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        gps = GPSTracker(context)

    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pemadam, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = PemadamPresenter(this)

        map.onCreate(savedInstanceState)
        map.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!

        val permissions = arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION)
        ActivityCompat.requestPermissions(activity!!, permissions,0)

        mMap.isMyLocationEnabled = true

        val circle = mMap!!.addCircle(
            CircleOptions()
                .center(LatLng(gps.latitude, gps.longitude))
                .radius(1500.0)
                .strokeColor(ContextCompat.getColor(context!!, R.color.colorPrimaryTrans))
                .fillColor(ContextCompat.getColor(context!!, R.color.colorPrimaryTrans))
        )
        circle.isVisible

        // Add a marker in Sydney, Australia, and move the camera.
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(gps.latitude, gps.longitude), 12f))
        mMap.addMarker(
            MarkerOptions().position(LatLng(gps.latitude, gps.longitude)).icon(Helpers.bitmapDescriptorFromVector(this.requireContext(),R.drawable.ic_loc_me)).title("Lokasi Saya")
        )

        presenter.hitDataPemadam()

//        mMap.setOnMarkerClickListener(this);

//        getDataBankSampah(1)
    }

    fun onResult(list: MutableList<DataPemadam>){
        list.forEach {
            mMap.addMarker(
                MarkerOptions().position(LatLng(it.LAT, it.LNG)).icon(Helpers.bitmapDescriptorFromVector(this.requireContext(),R.mipmap.ic_fire)).title(it.POS_PEMADAM).snippet(
                    it.ALAMAT
                )
            )
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (map != null){
            map.onDestroy()
        }
    }

    override fun onPause() {
        super.onPause()
        if (map != null){
            try {
                map.onPause()
            } catch (e:Exception){
                e.printStackTrace()
            }
        }
    }

    override fun onLowMemory() {
        super.onLowMemory()
        map.onLowMemory()
    }

    override fun onResume() {
        super.onResume()
        try {
            map.onResume()
        } catch (e:Exception){
            e.printStackTrace()
        }

    }

    override fun setMenuVisibility(menuVisible: Boolean) {
        if (menuVisible){
            try {
                map.onResume()
            } catch (e:Exception){
                e.printStackTrace()
            }
        }
        super.setMenuVisibility(menuVisible)
    }

}
