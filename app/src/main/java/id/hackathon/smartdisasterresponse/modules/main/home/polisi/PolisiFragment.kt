package id.hackathon.smartdisasterresponse.modules.main.home.polisi

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.common.api.GoogleApiClient

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import id.hackathon.smartdisasterresponse.R
import id.hackathon.smartdisasterresponse.models.response.polisi.DataPolisi
import id.hackathon.smartdisasterresponse.utils.GPSTracker
import kotlinx.android.synthetic.main.fragment_polisi.*
import id.hackathon.smartdisasterresponse.Helpers


class PolisiFragment : Fragment(), OnMapReadyCallback {

    private lateinit var presenter: PolisiPresenter

    private lateinit var gps: GPSTracker
    private lateinit var mGoogleApiClient: GoogleApiClient
    private val isGpsEnabled: Boolean = false
    private lateinit var mMap: GoogleMap


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        gps = GPSTracker(context)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_polisi, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter = PolisiPresenter(this)

        map.onCreate(savedInstanceState)
        map.getMapAsync(this)


    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!

        val permissions = arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION)
        ActivityCompat.requestPermissions(activity!!, permissions, 0)

        mMap.isMyLocationEnabled = true

        val circle = mMap!!.addCircle(
            CircleOptions()
                .center(LatLng(gps.latitude, gps.longitude))
                .radius(1500.0)
                .strokeColor(ContextCompat.getColor(context!!, R.color.colorPrimaryTrans))
                .fillColor(ContextCompat.getColor(context!!, R.color.colorPrimaryTrans))
        )
        circle.isVisible

        // Add a marker in Sydney, Australia, and move the camera.
//        val myLoc = LatLng(-6.213510, 106.802570)
//        mMap.addMarker(
//            MarkerOptions().position(myLoc).icon(Helpers.bitmapDescriptorFromVector(this.requireContext(),R.mipmap.ic_police)).title("Lokasi Saya").snippet(
//                "me"
//            )
//        )
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(gps.latitude, gps.longitude), 12f))
        mMap.addMarker(
            MarkerOptions().position(LatLng(gps.latitude, gps.longitude)).icon(Helpers.bitmapDescriptorFromVector(this.requireContext(),R.drawable.ic_loc_me)).title("Lokasi Saya")
        )

        presenter.hitDataPolisi()

//        mMap.setOnMarkerClickListener(this);

//        getDataBankSampah(1)
    }

    fun onResult(list: MutableList<DataPolisi>){
        list.forEach {
            mMap.addMarker(
                MarkerOptions().position(LatLng(it.lat, it.lng)).icon(Helpers.bitmapDescriptorFromVector(this.requireContext(),R.mipmap.ic_police)).title(it.name).snippet(
                    it.address
                )
            )
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (map != null) {
            map.onDestroy()
        }
    }

    override fun onPause() {
        super.onPause()
        if (map != null) {
            try {
                map.onPause()
            } catch (e:Exception){
                e.printStackTrace()
            }
        }
    }

    override fun onLowMemory() {
        super.onLowMemory()
        map.onLowMemory()
    }

    override fun onResume() {
        super.onResume()
        try {
            map.onResume()
        } catch (e:Exception){
            e.printStackTrace()
        }
    }

}
