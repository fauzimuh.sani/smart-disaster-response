package id.hackathon.smartdisasterresponse.modules.main.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import io.realm.Realm
import id.hackathon.smartdisasterresponse.R
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.toolbar.*


class HomeFragment: Fragment() {
    private lateinit var realm: Realm
    private var mCount = 20


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        realm = Realm.getDefaultInstance()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initWidget()
        setButton(bt_polisi)
//        badge_notif.setNumber(mCount)


    }


    private fun initWidget() {

//        profile.setOnClickListener{
//            val intent = Intent(this.requireContext(), MyProfileActivity::class.java)
//            startActivity(intent)
//        }

//        badge_notif.setOnClickListener{
//            val intent = Intent(this.requireContext(), NotificationActivity::class.java)
//            startActivity(intent)
//        }

//        Helpers.setCompoundIconLeft(et_search,context!!, CommunityMaterial.Icon.cmd_magnify)

        var myAdapter = HomePagerAdapter(this.childFragmentManager)
        vp_home.setAdapter(myAdapter);
        vp_home.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {


            }

            override fun onPageSelected(position: Int) { //this is manual listener for button (tab) when page is changed
                if (position == 0) {
                    setButton(bt_polisi)
                } else if (position == 1) {
                    setButton(bt_pemadam)
                } else if (position == 2) {
                    setButton(bt_rs)
                } else if (position == 3){
                    setButton(bt_ambulance)
                }
            }

        }
        )


        bt_polisi.setOnClickListener {
            setButton(bt_polisi)
            vp_home.setCurrentItem(0,true)
        }

        bt_pemadam.setOnClickListener {
            setButton(bt_pemadam)
            vp_home.setCurrentItem(1,true)
        }

        bt_rs.setOnClickListener {
            setButton(bt_rs)
            vp_home.setCurrentItem(2,true)
        }

        bt_ambulance.setOnClickListener {
            setButton(bt_ambulance)
            vp_home.setCurrentItem(3,true)
        }

    }

    private fun setButton(button: Button) {
        if (bt_rs.equals(button)) {
            bt_rs.setBackgroundResource(R.drawable.button_radius_gradient_blue)
            bt_rs.setTextColor(ContextCompat.getColor(this.requireContext(), R.color.white_ori))
            bt_pemadam.setBackgroundResource(R.drawable.button_radius_white)
            bt_pemadam.setTextColor(ContextCompat.getColor(this.requireContext(), R.color.color_grey))
            bt_polisi.setBackgroundResource(R.drawable.button_radius_white)
            bt_polisi.setTextColor(ContextCompat.getColor(this.requireContext(), R.color.color_grey))
            bt_ambulance.setBackgroundResource(R.drawable.button_radius_white)
            bt_ambulance.setTextColor(ContextCompat.getColor(this.requireContext(), R.color.color_grey))
        } else if (bt_pemadam.equals(button)) {
            bt_rs.setBackgroundResource(R.drawable.button_radius_white)
            bt_rs.setTextColor(ContextCompat.getColor(this.requireContext(), R.color.color_grey))
            bt_pemadam.setBackgroundResource(R.drawable.button_radius_gradient_blue)
            bt_pemadam.setTextColor(ContextCompat.getColor(this.requireContext(), R.color.white_ori))
            bt_polisi.setBackgroundResource(R.drawable.button_radius_white)
            bt_polisi.setTextColor(ContextCompat.getColor(this.requireContext(), R.color.color_grey))
            bt_ambulance.setBackgroundResource(R.drawable.button_radius_white)
            bt_ambulance.setTextColor(ContextCompat.getColor(this.requireContext(), R.color.color_grey))
        } else if (bt_polisi.equals(button)) {
            bt_rs.setBackgroundResource(R.drawable.button_radius_white)
            bt_rs.setTextColor(ContextCompat.getColor(this.requireContext(), R.color.color_grey))
            bt_pemadam.setBackgroundResource(R.drawable.button_radius_white)
            bt_pemadam.setTextColor(ContextCompat.getColor(this.requireContext(), R.color.color_grey))
            bt_polisi.setBackgroundResource(R.drawable.button_radius_gradient_blue)
            bt_polisi.setTextColor(ContextCompat.getColor(this.requireContext(), R.color.white_ori))
            bt_ambulance.setBackgroundResource(R.drawable.button_radius_white)
            bt_ambulance.setTextColor(ContextCompat.getColor(this.requireContext(), R.color.color_grey))
        } else if (bt_ambulance.equals(button)){
            bt_ambulance.setBackgroundResource(R.drawable.button_radius_gradient_blue)
            bt_ambulance.setTextColor(ContextCompat.getColor(this.requireContext(), R.color.white_ori))
            bt_pemadam.setBackgroundResource(R.drawable.button_radius_white)
            bt_pemadam.setTextColor(ContextCompat.getColor(this.requireContext(), R.color.color_grey))
            bt_polisi.setBackgroundResource(R.drawable.button_radius_white)
            bt_polisi.setTextColor(ContextCompat.getColor(this.requireContext(), R.color.color_grey))
            bt_rs.setBackgroundResource(R.drawable.button_radius_white)
            bt_rs.setTextColor(ContextCompat.getColor(this.requireContext(), R.color.color_grey))
        }
    }


}