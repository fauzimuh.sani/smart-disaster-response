package id.hackathon.smartdisasterresponse.modules.main.home.ambulance

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import id.hackathon.smartdisasterresponse.Helpers
import id.hackathon.smartdisasterresponse.R
import id.hackathon.smartdisasterresponse.models.response.ambulance.DATA
import id.hackathon.smartdisasterresponse.utils.GPSTracker
import kotlinx.android.synthetic.main.fragment_ambulance.*

class AmbulanceFragment : Fragment(), OnMapReadyCallback {

    private lateinit var gps: GPSTracker
    private lateinit var presenter: AmbulancePresenter
    private lateinit var mMap: GoogleMap
    private var listData: MutableList<DATA> = mutableListOf()
    private var markerOptionsList: MutableList<MarkerOptions> = mutableListOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        gps = GPSTracker(context)

    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ambulance, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = AmbulancePresenter(this)

        map.onCreate(savedInstanceState)
        map.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!

        val permissions = arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION)
        ActivityCompat.requestPermissions(activity!!, permissions, 0)

        mMap.isMyLocationEnabled = true

        val circle = mMap!!.addCircle(
            CircleOptions()
                .center(LatLng(gps.latitude, gps.longitude))
                .radius(1500.0)
                .strokeColor(ContextCompat.getColor(context!!, R.color.colorPrimaryTrans))
                .fillColor(ContextCompat.getColor(context!!, R.color.colorPrimaryTrans))
        )
        circle.isVisible

        // Add a marker in Sydney, Australia, and move the camera.
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(gps.latitude, gps.longitude), 12f))
        mMap.addMarker(
            MarkerOptions().position(
                LatLng(
                    gps.latitude,
                    gps.longitude
                )
            ).icon(Helpers.bitmapDescriptorFromVector(this.requireContext(), R.drawable.ic_loc_me)).title("Lokasi Saya")
        )
        mMap.setOnInfoWindowClickListener { marker ->
            this.markerOptionsList.forEach { data ->
                if (data.position.toString().contains(marker.position.toString())) {
                    val builder = AlertDialog.Builder(context)

                    builder.setMessage("Apakah anda ingin menghubungi " + listData[this.markerOptionsList.indexOf(data)].OWNERNAME + " ?")
                        .setPositiveButton("Iya") { dialog, id ->
                            val callIntent = Intent(Intent.ACTION_CALL)
                            callIntent.data = Uri.parse(
                                "tel:" + "0212340987"
                            )

                            if (ActivityCompat.checkSelfPermission(
                                    this@AmbulanceFragment.requireContext(),
                                    Manifest.permission.CALL_PHONE
                                ) !== PackageManager.PERMISSION_GRANTED
                            ) {
                                return@setPositiveButton
                            }
                            (context as Activity).startActivity(callIntent)

                        }
                        .setNegativeButton("Tidak", DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })
                        .show()
//                    Toast.makeText(this@AmbulanceFragment.requireContext(), listData.get(this.markerOptionsList.indexOf(data)).VEHICLE_STATE, Toast.LENGTH_LONG).show()
                }
            }


        }
        presenter.hitDataAmbulance()


//        mMap.setOnMarkerClickListener(this);

//        getDataBankSampah(1)
    }

    fun onResult(list: MutableList<DATA>) {
        this.listData.clear()
        this.listData.addAll(list)
        list.forEach {
            val marker = MarkerOptions().position(
                LatLng(
                    it.LATITUDE.toDouble(),
                    it.LONGITUDE.toDouble()
                )
            ).icon(
                Helpers.bitmapDescriptorFromVector(
                    this.requireContext(),
                    R.mipmap.ic_ambulance
                )
            ).title(it.OWNERNAME).snippet(
                it.ADDRESS
            )
            mMap.addMarker(
                marker
            )
            this.markerOptionsList.add(marker)
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        if (map != null) {
            map.onDestroy()
        }
    }

    override fun onPause() {
        super.onPause()
        if (map != null) {
            try {
                map.onPause()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onLowMemory() {
        super.onLowMemory()
        map.onLowMemory()
    }

    override fun onResume() {
        super.onResume()
        try {
            map.onResume()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun setMenuVisibility(menuVisible: Boolean) {
        if (menuVisible) {
            try {
                map.onResume()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        super.setMenuVisibility(menuVisible)
    }

}
